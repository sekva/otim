extern crate num;
extern crate rand;

use std::fmt;
use std::ops::Add;

const DBL_EPS: f64 = 1e-12;
const TOL: f64 = 0.01;
const PASSO: f64 = 1e-3;

const A: f64 = 1.0;
const B: f64 = 100.0;

fn derive<F>(x: Vec<f64>, f: F) -> Vec<f64>
where
    F: Fn(Vec<f64>) -> f64,
{
    let mut xr = Vec::new();

    for i in 0..x.len() {
        let mut x1 = x.clone();
        let mut x2 = x.clone();

        x1[i] -= DBL_EPS;
        x2[i] += DBL_EPS;

        let y1 = f(x1.clone());
        let y2 = f(x2.clone());

        xr.push((y2 - y1) / (x2[i] - x1[i]));
    }

    return xr;
}

fn derive_ros(x: Vec<f64>) -> Vec<f64> {
    vec![
        -2.0 * A + 4.0 * B * x[0].powi(3) - 4.0 * B * x[0] * x[1] + 2.0 * x[0],
        2.0 * B * (x[1] - x[0].powi(2)),
    ]
}

fn rosenbrock(x: Vec<f64>) -> f64 {
    (A - x[0]).powi(2) + B * (x[1] - x[0].powi(2)).powi(2)
}

fn levy_13(x: Vec<f64>) -> f64 {
    let termo1 = ((3.0 * 3.1415926 * x[0]).sin()).powi(2);
    let termo2 = (x[0] - 1.0).powi(2) * (1.0 + ((3.0 * 3.1415926 * x[1]).sin()).powi(2));
    let termo3 = (x[1] - 1.0).powi(2) * (1.0 + ((2.0 * 3.1415926 * x[1]).sin()).powi(2));

    termo1 + termo2 + termo3
}

fn norma(x: Vec<f64>) -> f64 {
    let mut soma = 0.0;

    for i in x {
        soma += i * i;
    }

    soma.sqrt()
}

fn checar_tolerancia(grad: Vec<f64>, tolerancia: f64) -> bool {
    grad.iter().all(|dx| dx.abs() <= tolerancia)
}

pub struct Resposta {
    iteracoes: u64,
    posicao: Vec<f64>,
    valor: f64,
    ak: f64,
    grad: f64,
}

impl std::fmt::Display for Resposta {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Iteração: {:9}, Ponto de minima: ({:.20}, {:.20}), Valor: {:.20}, AK: {:.20}, Grad: {:.20}",
            self.iteracoes, self.posicao[0], self.posicao[1], self.valor, self.ak, self.grad
        )
    }
}

fn armijo_line_search<F>(x: Vec<f64>, direcao: Vec<f64>, grad: Vec<f64>, f: F) -> f64
where
    F: Fn(Vec<f64>) -> f64,
{
    let controle = 0.5;
    let inicial = 1.0;
    let fator = 0.5;

    let m = grad
        .iter()
        .zip(direcao.clone())
        .map(|(g, d)| g * d)
        .fold(0.0, Add::add);

    let t = -controle * m;

    assert!(t > 0.0);

    let mut ak = inicial;

    loop {
        let mut pos_teste = Vec::new();

        for i in 0..direcao.len() {
            pos_teste.push(x[i] + (ak * direcao[i]));
        }

        if f(pos_teste) < (f(x.clone()) - ak * t) {
            return ak;
        }

        ak = ak * fator;
    }
}

fn line_search<F>(x: Vec<f64>, direcao: Vec<f64>, f: F) -> f64
where
    F: Fn(Vec<f64>) -> f64,
{
    let mut ak = std::f64::MAX;
    let mut contador = 0;
    loop {
        if contador > 1000000 {
            return ak;
        } else {
        }

        let mut pos_teste = Vec::new();

        for i in 0..direcao.len() {
            pos_teste.push(x[i] + (ak * direcao[i]));
        }

        if f(pos_teste) < f(x.clone()) {
            // decrementa ak ate achar o maior valor q satisfaz
            ak = ak * 2 as f64;

            loop {
                ak = ak - 0.0001;
                pos_teste = Vec::new();

                for i in 0..direcao.len() {
                    pos_teste.push(x[i] + (ak * direcao[i]));
                }

                if f(pos_teste) < f(x.clone()) {
                    return ak;
                }
            }
        } else {
            ak = ak / 2 as f64;
        }
        contador += 1;
    }
}

fn steepest<F>(x: Vec<f64>, f: F) -> Resposta
where
    F: Fn(Vec<f64>) -> f64,
{
    let mut posicao = x;
    let mut valor = f(posicao.clone());

    let mut contador = 0;

    loop {
        let gradiente = derive(posicao.clone(), &f);

        if checar_tolerancia(gradiente.clone(), TOL) {
            return Resposta {
                iteracoes: contador,
                posicao: posicao,
                valor: valor,
                ak: 0.0,
                grad: norma(gradiente),
            };
        }

        let direcao: Vec<f64> = gradiente.clone().into_iter().map(|xn| -xn).collect();

        let ak = armijo_line_search(posicao.clone(), direcao.clone(), gradiente.clone(), &f);

        for i in 0..direcao.len() {
            posicao[i] = posicao[i] + (ak * direcao[i]);
        }

        valor = f(posicao.clone());
        contador += 1;

        if contador >= std::u64::MAX {
            return Resposta {
                iteracoes: contador,
                posicao: posicao,
                valor: valor,
                ak: ak,
                grad: norma(gradiente),
            };
        }

        print!(
            "{}\r",
            Resposta {
                iteracoes: contador,
                posicao: posicao.clone(),
                valor: valor,
                ak: ak,
                grad: norma(gradiente),
            }
        );
    }
}

fn main() {
    let mut pos = 9.5;

    while pos >= 0.0 {
        let asd = vec![pos, pos];
        println!();
        println!();
        println!();
        println!();
        println!("pos {}", pos);
        println!("{:?}", derive(asd.clone(), levy_13));

        println!("{}", steepest(asd, levy_13));
        pos -= 0.5;
    }
}
